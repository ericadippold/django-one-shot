# Generated by Django 4.0.3 on 2022-05-03 18:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todos', '0003_todoitem_due_date_todoitem_is_completed_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todoitem',
            name='due_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
