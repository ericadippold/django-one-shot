from django.urls import path

from todos.views import TodoDeleteView, TodoDetailView, TodoItemCreateView, TodoItemUpdateView, TodoListView, TodoCreateView, TodoUpdateView

urlpatterns = [
    path("", TodoListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodoCreateView.as_view(), name="todo_list_create"),
    path("<int:pk>/edit/", TodoUpdateView.as_view(), name="todo_list_edit"),
    path("<int:pk>/delete/", TodoDeleteView.as_view(), name="todo_list_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="todo_item_create"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todo_item_edit"),
]
